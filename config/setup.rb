require "bundler/setup"
require "dotenv"
require "active_support/time"
require "byebug"

require "./lib/logger"

Dotenv.load

ENV["APP_ENV"] ||= "development"

Bundler.require(:default, ENV["APP_ENV"].to_sym)
