require "./lib/loader/entries"

describe Loader::Entries do
  let(:pis_number) { "59417640765" }

  let(:entry_dates) do
    [
      "2018-04-28T05:43:00",
      "2018-04-28T07:43:00",
      "2018-04-30T05:43:00"
    ]
  end

  subject { described_class.new("./spec/fixtures/timeclock_entries.json") }

  let(:expected_input_entries) do
    [{ "pis_number" => pis_number, "entries" => entry_dates }]
  end

  it "loads entries file" do
    expect(subject.input_entries).to eq expected_input_entries
  end

  describe "#load" do
    let(:grouped_entries) do
      {
        "2018-04-28T00:00:00".to_time => [
          "2018-04-28T05:43:00".to_time,
          "2018-04-28T07:43:00".to_time
        ],
        "2018-04-30T00:00:00".to_time => [
          "2018-04-30T05:43:00".to_time
        ]
      }
    end

    it "groups entries by day" do
      subject.load

      expect(subject.input_entries.first["daily_entries"]).to eq grouped_entries
    end
  end
end
