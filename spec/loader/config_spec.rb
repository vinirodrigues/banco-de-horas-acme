require "./lib/loader/config"

describe Loader::Config do
  let(:today) { "2018-05-07" }
  let(:period_start) { "2018-04-10" }

  let(:expected_config) do
    {
      "today" => today,
      "period_start" => period_start,
      "employees" => [
        {
          "workload" => [
            {
              "workload_in_minutes" => 300,
              "minimum_rest_interval_in_minutes" => 15,
              "days" => [
                "mon",
                "tue",
                "wed",
                "thu",
                "fri"
              ]
            }
          ],
          "pis_number" => "59417640765",
          "name" => "Goddard Lewis Son"
        }
      ]
    }
  end

  subject { described_class.new("./spec/fixtures/config.json") }

  it "loads config file" do
    expect(subject.config).to eq expected_config
  end

  describe "#load" do
    it "loads start at time" do
      expect(subject.load[:start_at]).to eq period_start.to_time
    end

    it "loads end at time" do
      expect(subject.load[:end_at]).to eq today.to_time
    end

    it "loads employees" do
      expect(subject.load[:employees]).to eq expected_config["employees"]
    end
  end
end
