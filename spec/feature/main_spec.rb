require "./lib/main"

describe "Main feature" do
  let(:generated_response) { File.read "./support/response.json" }
  let(:expected_response) { File.read "./spec/fixtures/response.json" }

  it "generates correct response file" do
    Main.init

    expect(generated_response).to eq expected_response
  end
end
