require "./lib/employee/builder"

describe Employee::Builder do
  let(:pis_number) { "123" }
  let(:name) { "John Doe" }
  let(:workload_in_minutes) { 300 }
  let(:minimum_rest_interval_in_minutes) { 15 }

  let(:employees_input) do
    [
      {
        "workload" => [
          {
            "workload_in_minutes" => workload_in_minutes,
            "minimum_rest_interval_in_minutes" => minimum_rest_interval_in_minutes,
            "days" => [
              "mon",
              "tue",
              "wed",
              "thu",
              "fri"
            ]
          }
        ],
        "pis_number" => pis_number,
        "name" => name
      }
    ]
  end

  subject { described_class.new(employees_input) }

  let(:expected_employee_attr) do
    { pis_number: pis_number, name: name, daily_entries: [] }
  end

  let(:expected_workload_attr) do
    {
      minutes: workload_in_minutes,
      minimum_rest: minimum_rest_interval_in_minutes,
      days: [1, 2, 3, 4, 5]
    }
  end

  it "builds employee" do
    subject.build

    expect(subject.employees.first).to have_attributes expected_employee_attr
  end

  it "builds workload" do
    subject.build

    expect(
      subject.employees.first.workloads.first
    ).to have_attributes expected_workload_attr
  end
end
