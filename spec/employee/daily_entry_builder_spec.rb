require "./lib/employee/daily_entry_builder"
require "./lib/employee"

describe Employee::DailyEntryBuilder do
  let(:employee) { Employee.new(pis_number: nil, name: nil, workloads: nil) }
  let(:start_at) { "2018-04-29".to_time }
  let(:end_at) { "2018-04-29".to_time }

  let(:input_entries) do
    [
      ["2018-04-28".to_time, ["09:00:00".to_time]],
      ["2018-04-29".to_time, ["09:00:00".to_time]],
      ["2018-04-30".to_time, ["09:00:00".to_time]],
    ]
  end

  subject do
    described_class.new(
      employee: employee,
      start_at: start_at,
      end_at: end_at,
      input_entries: input_entries
    )
  end

  describe "#build" do
    let(:expected_attributes) do
      { day: "2018-04-29".to_time, entries: ["09:00:00".to_time] }
    end

    it "builds valid daily entries to employee with right attributes" do
      subject.build

      expect(employee.daily_entries.first).to have_attributes expected_attributes
    end

    it "attaches to employee only valid daily entries" do
      subject.build

      expect(employee.daily_entries.count).to eq 1
    end
  end
end
