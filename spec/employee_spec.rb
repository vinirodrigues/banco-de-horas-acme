require "./lib/employee"
require "./lib/daily_entry"

describe Employee do
  let(:workload) do
    Workload.new({
      "workload_in_minutes" => 480,
      "minimum_rest_interval_in_minutes" => 60,
      "days" => ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
    })
  end

  let(:workaholic_entries) do
    [
      "09:00:00",
      "12:00:00",
      "13:00:00",
      "23:00:00"
    ].map(&:to_time)
  end

  let(:lazy_entries) do
    [
      "11:00:00",
      "12:00:00",
      "13:00:00",
      "18:00:00"
    ].map(&:to_time)
  end

  let(:workaholic_day) do
    DailyEntry.new(day: "2018-04-28".to_time, entries: workaholic_entries)
  end

  let(:lazy_day) do
    DailyEntry.new(day: "2018-04-29".to_time, entries: lazy_entries)
  end

  subject do
    described_class.new(
      pis_number: "123",
      name: "John Doe",
      workloads: [workload]
    )
  end

  before do
    subject.daily_entries = [workaholic_day, lazy_day]
  end

  describe "#daily_balance" do
    it{ expect(subject.daily_balance).to eq [300.0, -120.0] }
  end

  describe "#summary_balance" do
    it{ expect(subject.summary_balance).to eq 180.0 }
  end

  describe "#to_response" do
    let(:expected_response) do
      {
        pis_number: "123",
        summary: { balance: 180.0 },
        history: [
          { day: "2018-04-28", balance: 300 },
          { day: "2018-04-29", balance: -120.0 }
        ]
      }
    end

    it{ expect(subject.to_response).to eq expected_response }
  end
end
