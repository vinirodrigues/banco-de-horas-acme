require "./lib/daily_entry"

describe DailyEntry do
  let(:day) { "2018-04-28" }

  let(:entries) do
    [
      "09:00:00",
      "12:00:00",
      "13:00:00",
      "18:00:00"
    ].map(&:to_time)
  end

  subject { described_class.new(day: day.to_time, entries: entries) }

  describe "#worked_minutes" do
    it "calculates worked time in one day" do
      expect(subject.worked_minutes).to eq 480.0
    end
  end

  describe "#to_response" do
    it{ expect(subject.to_response).to eq day }
  end
end
