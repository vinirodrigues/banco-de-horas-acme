require "./lib/main.rb"

describe Main do
  describe ".init" do
    let(:request_instance) do
      double Timeclock::Request, employees: [], from_json: nil
    end

    let(:response_instance) { double Timeclock::Response }

    context "when loading data from json" do
      it "calls timeclock request" do
        expect(Timeclock::Request).to receive(:new).with(
          config_file: "./support/config.json",
          entries_file: "./support/timeclock_entries.json"
        ).and_return(request_instance)

        expect(request_instance).to receive(:from_json)

        described_class.init
      end
    end

    context "when generating response json" do
      before do
        allow(Timeclock::Request).to receive(:new).and_return(request_instance)
      end

      it "calls timeclock response" do
        expect(Timeclock::Response).to receive(:new).with(
          employees: request_instance.employees,
          response_file: "./support/response.json"
        ).and_return(response_instance)

        expect(response_instance).to receive(:to_json)

        described_class.init
      end
    end
  end
end
