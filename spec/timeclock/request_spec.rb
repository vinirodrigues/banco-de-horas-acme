require "./lib/timeclock/request"

describe Timeclock::Request do
  let(:config_file) { "config.json" }
  let(:entries_file) { "entries.json" }

  let(:config_loader) { double Loader::Config }
  let(:entries_loader) { double Loader::Entries }

  let(:pis_number) { "123" }
  let(:start_at) { "2018-04-29" }
  let(:end_at) { "2018-04-30" }

  let(:employees) do
    [
      {
        "workload" => [
          {
            "workload_in_minutes" => 300,
            "minimum_rest_interval_in_minutes" => 15,
            "days" => [
              "mon",
              "tue",
              "wed",
              "thu",
              "fri"
            ]
          }
        ],
        "pis_number" => pis_number,
        "name" => "John Doe"
      }
    ]
  end

  let(:config_loader_response) do
    { employees: employees, start_at: start_at, end_at: end_at }
  end

  let(:entries_loader_response) do
    [
      { "pis_number" => pis_number, "daily_entry" => "entry1" },
      { "pis_number" => pis_number, "daily_entry" => "entry2" }
    ]
  end

  let(:employee_builder) { double Employee::Builder, employees: [] }
  let(:daily_entry_builder) { double Employee::DailyEntryBuilder }

  before do
    allow(Loader::Config).to receive(:new)
      .with(config_file)
      .and_return(config_loader)

    allow(config_loader).to receive(:load).and_return(config_loader_response)

    allow(Loader::Entries).to receive(:new)
      .with(entries_file)
      .and_return(entries_loader)

    allow(entries_loader).to receive(:load).and_return(entries_loader_response)

    allow(Employee::Builder).to receive(:new)
      .with(employees)
      .and_return(employee_builder)

    allow(employee_builder).to receive(:build)

    allow(Employee::DailyEntryBuilder).to receive(:new)
      .and_return(daily_entry_builder)

    allow(daily_entry_builder).to receive(:build)
  end

  it "calls config loader" do
    expect(config_loader).to receive(:load)

    described_class.new(config_file: config_file, entries_file: entries_file)
  end

  it "calls entries loader" do
    expect(entries_loader).to receive(:load)

    described_class.new(config_file: config_file, entries_file: entries_file)
  end

  subject do
    described_class.new(config_file: config_file, entries_file: entries_file)
  end

  describe "#from_json" do
    it "calls employee builder" do
      expect(employee_builder).to receive(:build)

      subject.from_json
    end

    it "calls daily entries builder to each loaded entry" do
      expect(daily_entry_builder).to receive(:build).twice

      subject.from_json
    end
  end

  describe "#employees" do
    it{ expect(subject.employees).to eq employee_builder.employees }
  end

  describe "#start_at" do
    it{ expect(subject.start_at).to eq start_at }
  end

  describe "#employees" do
    it{ expect(subject.end_at).to eq end_at }
  end
end
