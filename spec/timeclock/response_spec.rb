require "./lib/timeclock/response"

describe Timeclock::Response do
  let(:response_file) { "./spec/fixtures/timeclock/response_spec.json" }

  let(:workload) do
    Workload.new({
      "workload_in_minutes" => 480,
      "minimum_rest_interval_in_minutes" => 60,
      "days" => ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
    })
  end

  let(:entries) do
    [
      "09:00:00",
      "12:00:00",
      "13:00:00",
      "23:00:00"
    ].map(&:to_time)
  end

  let(:daily_entry) do
    DailyEntry.new(day: "2018-04-29".to_time, entries: entries)
  end

  let(:employee) do
    Employee.new(
      pis_number: "123",
      name: "John Doe",
      workloads: [ workload ]
    )
  end

  before do
    employee.daily_entries << daily_entry
  end

  subject do
    described_class.new(
      employees: [ employee ],
      response_file: response_file
    )
  end

  describe "#to_json" do
    let(:generated_response) { File.read response_file }

    let(:expected_response) do
      '[{"pis_number":"123","summary":{"balance":300.0},' \
      '"history":[{"day":"2018-04-29","balance":300.0}]}]'
    end

    it "generates json response file" do
      subject.to_json

      expect(generated_response).to eq expected_response
    end
  end
end
