require "./lib/workload"

describe Workload do
  let(:minutes) { 100 }
  let(:minimum_rest) { 10 }

  let(:params) do
    {
      "workload_in_minutes" => minutes,
      "minimum_rest_interval_in_minutes" => minimum_rest,
      "days" => ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
    }
  end

  subject { described_class.new(params) }

  it "parses minutes from params" do
    expect(subject.minutes).to eq minutes
  end

  it "parses minimum rest from params" do
    expect(subject.minimum_rest).to eq minimum_rest
  end

  it "parses days from params" do
    expect(subject.days).to eq [0, 1, 2, 3, 4, 5, 6]
  end
end
