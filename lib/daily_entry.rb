class DailyEntry
  attr_reader :day, :entries, :worked_minutes

  def initialize(day:, entries:)
    @day = day
    @entries = entries
    @worked_minutes = calculate_worked_minutes
  end

  def to_response
    day.strftime("%Y-%m-%d")
  end

  private

  def calculate_worked_minutes
    minutes = 0

    entries.each_with_index do |entry_time, position|
      minutes += entries[position+1] - entry_time if not_an_exit?(position)
    end

    minutes / 60.0
  end

  def not_an_exit?(position)
    entries[position+1].present? && position.even?
  end
end
