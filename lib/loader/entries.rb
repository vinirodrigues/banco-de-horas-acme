require "json"

module Loader
  class Entries
    attr_reader :input_entries

    def initialize(filename)
      Logger.log "* Reading entries file on: #{filename}"

      @input_entries = JSON.parse(File.read filename)
    end

    def load
      input_entries.each do |input|
        input["daily_entries"] = group_by_day input["entries"]
      end
    end

    private

    def group_by_day(input_entry)
      input_entry.map(&:to_time).group_by(&:beginning_of_day)
    end
  end
end
