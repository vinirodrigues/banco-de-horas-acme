require "json"

module Loader
  class Config
    attr_reader :config

    def initialize(filename)
      Logger.log "* Reading config file on: #{filename}"

      @config = JSON.parse(File.read filename)
    end

    def load
      {
        start_at: config["period_start"].to_time,
        end_at: config["today"].to_time,
        employees: config["employees"]
      }
    end
  end
end
