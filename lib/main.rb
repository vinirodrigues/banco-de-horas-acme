require_relative "timeclock/request"
require_relative "timeclock/response"

class Main
  def self.init
    request = Timeclock::Request.new(
      config_file: "./support/config.json",
      entries_file: "./support/timeclock_entries.json"
    )
    request.from_json

    Timeclock::Response.new(
      employees: request.employees,
      response_file: "./support/response.json"
    ).to_json
  end
end
