require_relative "../loader/config"
require_relative "../loader/entries"
require_relative "../employee/builder"
require_relative "../employee/daily_entry_builder"

module Timeclock
  class Request
    attr_reader :config_file, :entries_file

    def initialize(config_file:, entries_file:)
      Logger.log "Requesting timeclock.."

      @config_file = config_file
      @entries_file = entries_file

      load_config_from_json
      load_entries_from_json
    end

    def from_json
      employees_builder.build

      build_daily_entries
    end

    def employees
      employees_builder.employees
    end

    def start_at
      load_config_from_json[:start_at]
    end

    def end_at
      load_config_from_json[:end_at]
    end

    private

    def load_config_from_json
      @config_loader ||= Loader::Config.new(config_file).load
    end

    def load_entries_from_json
      @entries_loader ||= Loader::Entries.new(entries_file).load
    end

    def employees_builder
      @employees_builder ||= Employee::Builder.new(
        load_config_from_json[:employees]
      )
    end

    def build_daily_entries
      @daily_entries_builder ||= load_entries_from_json.each do |entry|
        Employee::DailyEntryBuilder.new(
          employee: find_employee(entry),
          start_at: start_at,
          end_at: end_at,
          input_entries: entry["daily_entries"]
        ).build
      end
    end

    def find_employee(entry)
      employees.find{ |e| e.pis_number == entry["pis_number"] }
    end
  end
end
