module Timeclock
  class Response
    attr_reader :employees, :response_file

    def initialize(employees:, response_file:)
      Logger.log "Responding timeclock on: #{response_file}"

      @employees = employees
      @response_file = response_file
    end

    def to_json
      File.open(response_file,"w"){ |file| file.write(body) }
    end

    private

    def body
      employees.map(&:to_response).to_json
    end
  end
end
