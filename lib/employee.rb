require_relative "workload"

class Employee
  attr_reader :pis_number, :name, :workloads
  attr_accessor :daily_entries

  def initialize(pis_number:, name:, workloads:)
    @pis_number = pis_number
    @name = name
    @workloads = workloads
    @daily_entries = []
  end

  def daily_balance
    daily_entries.map do |daily_entry|
      workload = workload_by(daily_entry.day)

      if workload.nil?
        daily_entries.delete(daily_entry)
        next
      end

      daily_entry.worked_minutes - workload.minutes
    end.compact
  end

  def summary_balance
    daily_balance.reduce(:+)
  end

  def to_response
    {
      pis_number: pis_number,
      summary: { balance: summary_balance },
      history: daily_balance_response
    }
  end

  private

  def workload_by(day)
    workloads.find{ |workload| workload.days.include? day.wday }
  end

  def daily_balance_response
    entries_and_balances = daily_entries.map(&:to_response).zip daily_balance

    entries_and_balances.map do |response|
      { day: response.first, balance: response.last }
    end
  end
end
