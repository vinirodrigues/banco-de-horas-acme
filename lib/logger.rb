class Logger
  def self.log(message)
    puts message unless ENV["APP_ENV"] == "test"
  end
end
