class Workload
  WEEK_DAYS = {
    sun: 0,
    mon: 1,
    tue: 2,
    wed: 3,
    thu: 4,
    fri: 5,
    sat: 6
  }.freeze

  attr_reader :minutes, :minimum_rest, :days

  def initialize(params)
    @minutes = params["workload_in_minutes"]
    @minimum_rest = params["minimum_rest_interval_in_minutes"]
    @days = parse_days(params["days"])
  end

  private

  def parse_days(days_params)
    days_params.map{ |day| WEEK_DAYS[day.to_sym] }
  end
end
