require_relative "../daily_entry"

class Employee
  class DailyEntryBuilder
    attr_reader :employee, :start_at, :end_at, :input_entries

    def initialize(employee:, start_at:, end_at:, input_entries:)
      @employee = employee
      @start_at = start_at
      @end_at = end_at
      @input_entries = input_entries
    end

    def build
      input_entries.each do |entry|
        daily_entry = DailyEntry.new(day: entry.first, entries: entry.last)

        if daily_entry.day.between?(start_at, end_at)
          employee.daily_entries << daily_entry
        end
      end
    end
  end
end
