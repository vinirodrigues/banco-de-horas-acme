require_relative "../employee"
require_relative "../workload"

class Employee
  class Builder
    attr_reader :employees_input, :employees

    def initialize(employees_input)
      @employees_input = employees_input
    end

    def build
      @employees = employees_input.map do |employee|
        Employee.new(
          pis_number: employee["pis_number"],
          name: employee["name"],
          workloads: build_workloads(employee["workload"])
        )
      end
    end

    private

    def build_workloads(input_workloads)
      input_workloads.map{ |input| Workload.new(input) }
    end
  end
end
