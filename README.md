# Desafio "Banco de horas ACME"
----
## Rodando
Foi utilizado o Ruby na vers�o 2.4.1. As depend�ncias do projeto foram instaladas pelo Bundler.  
Para instal�-las, rode o seguinte comando dentro do diret�rio do projeto:

    
    bundle install

Para rodar o projeto, utilize o seguinte comando no seu diret�rio:

    
    ./bin/run

O arquivo de resposta ser� gravado dentro do projeto no path:
*support/response.json*  
Para rodar os testes, utilize o seguinte comando no diret�rio do projeto:
    
    
    rspec
